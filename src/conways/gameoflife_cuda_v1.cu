/**
* CAD 2015/2016 - TP2
*
* Luis Silva,     nr 44890
* Ricardo Gaspar, nr 42038
*
* Conway's Game of Life
* Parallel Version using CUDA C - Version 1:
* The cells in the border of the grid are always considered to be dead.
* Grids fit in  GPU memory.
*/

#include "file2ppm.c"
#include "sched.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

// Grid size
#define DEFAULT_NROWS 2048
#define DEFAULT_NCOLUMNS 2048
// number of time steps
#define DEFAULT_NSTEPS 100
// Generate image every IMAGE_INTERVAL
#define IMAGE_INTERVAL 10

// Theses constants define the number of rows and columms used for CUDA grid and
// blocks.
#define ROW_BLOCKS_PER_GRID 2
#define ROW_THREADS_PER_BLOCK 8

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

enum CELL_STATES { DEAD = 0, ALIVE };

int thread_count = 1;

char *programName;

void clearBorder(int *grid, int nrows, int ncolumns) {
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  // left-right border
  for (int i = 0; i < nrows; i++) {
    int index1 = i * ncolumns + 0;
    int index2 = i * ncolumns + endColumn;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }

  // top-bottom border
  for (int j = 0; j < ncolumns; j++) {
    int index1 = 0 * ncolumns + j;
    int index2 = endRow * ncolumns + j;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }
}

/*  randomly generate values DEAD(0) or ALIVE (1) to each cell of the grid */
void generateGrid(int *grid, int nrows, int ncolumns) {
  clearBorder(grid, nrows, ncolumns);
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  srand(time(0)); // use current time as seed for random generator
  for (int i = 1; i < endRow; i++) {
    for (int j = 1; j < endColumn; j++) {
      int index = i * ncolumns + j;
      grid[index] = rand() % 2;
    }
  }
}

__global__ void computeNextGridKernel(int *d_current_grid, int *d_next_grid,
                                      int nrows, int ncolumns) {
  int matrixSize = nrows * ncolumns;

  // The for cycle starts at (blockIdx.x * blockDim.x + threadIdx.x) + ncolumns
  // to jump the first row of the grid which is the border, also avoids the last
  // row matrixSize - ncolumns
  for (int index = (blockIdx.x * blockDim.x + threadIdx.x) + ncolumns;
       index < matrixSize - ncolumns; index += blockDim.x * gridDim.x) {
    int x = index % ncolumns;

    // Ignore the border columms
    if (x != 0 && x != ncolumns - 1) {
      int yUp = index - ncolumns;
      int yDown = index + ncolumns;
      int yUpLeft = yUp - 1;
      int yUpRight = yUp + 1;
      int yDownLeft = yDown - 1;
      int yDownRight = yDown + 1;
      int xLeft = index - 1;
      int xRight = index + 1;

      int nsum = d_current_grid[yUpLeft] + d_current_grid[yUp] +
                 d_current_grid[yUpRight] + d_current_grid[xLeft] +
                 d_current_grid[xRight] + d_current_grid[yDownLeft] +
                 d_current_grid[yDown] + d_current_grid[yDownRight];

      switch (nsum) {
      case 3:
        d_next_grid[index] = ALIVE;
        break;

      case 2:
        d_next_grid[index] = d_current_grid[index];
        break;

      default:
        d_next_grid[index] = DEAD;
      }
    }
  }
}

size_t getFreeBytes() {
  size_t free_bytes;
  size_t total_bytes;

  cudaError_t err = cudaMemGetInfo(&free_bytes, &total_bytes);
  if (err != cudaSuccess) {
    printf("Not enough available memory in GPU!\n");
    exit(1);
  }
  return free_bytes;
}

int main(int argc, char *argv[]) {

  struct timeval startTime;
  struct timeval stopTime;
  float elapsedTimeMilliSec, elapsedTimeSec;

  if (argc < 5) {
    printf("Not enough arguments! %d\n", argc);
    printf(
        "Usage: %s numberOfGridRows numberOfGridColumns "
        "numberOfSimulationSteps numberOfThreads [initialGridStateFile.csv]\n",
        argv[0]);
    return 0;
  }

  programName = argv[0];
  int initRows = atoi(argv[1]);
  int initColumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  thread_count = atoi(argv[4]);

  int nrows, ncolumns;

  int *h_current_grid;
  int *d_current_grid, *d_next_grid;

  // CUDA Initialization
  cudaError_t err;
  err = cudaSetDevice(0);
  if (err != cudaSuccess) {
    printf("ERROR!: cudaSetDevice\n");
    exit(1);
  }

  // CUDA Check free memory
  long free_bytes;
  free_bytes = getFreeBytes();
  if (free_bytes < 3 * (initRows * initColumns * sizeof(int))) {
    printf("CUDA Not enough memory to run! \n");
    printf("Execution aborted. \n");
    exit(1);
  }
  printf("CUDA: FreeMem :  %ld\n", free_bytes);

  int grid = ROW_BLOCKS_PER_GRID * ROW_BLOCKS_PER_GRID;
  int block = ROW_THREADS_PER_BLOCK * ROW_THREADS_PER_BLOCK;

  // allocate arrays
  nrows = initRows;       // add 2 for left and right ghost cells
  ncolumns = initColumns; // add 2 for top and bottom ghost cells
  int gridTopRow = 1, gridBottomRow = nrows - 2;
  int gridLeftColumn = 1, gridRightColumn = ncolumns - 2;

  gettimeofday(&startTime, 0);

  h_current_grid = (int *)malloc(nrows * ncolumns * sizeof(int));

  err = cudaMalloc((void **)&d_current_grid, nrows * ncolumns * sizeof(int));
  if (err != cudaSuccess) {
    printf("ERROR!: cudaMalloc d_current_grid \n");
    exit(1);
  }

  err = cudaMalloc((void **)&d_next_grid, nrows * ncolumns * sizeof(int));
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    printf("ERROR!: cudaMalloc d_next_grid\n");
    exit(1);
  }

  // populate initial grid
  if (argc == 6) {
    char *initialGridFileName = argv[5];
    CSVfileToGrid(initialGridFileName, h_current_grid, nrows, ncolumns,
                  WITH_BORDER);
    clearBorder(h_current_grid, nrows, ncolumns);
  } else {
    generateGrid(h_current_grid, nrows, ncolumns);
  }
  // clearBorder(next_grid, nrows, ncolumns);

  // create matrix file in CSV or Matrix
  // gridToCSVFile(programName, h_current_grid, 0, nrows, ncolumns,
  // WITH_BORDER); //TODO COMMENT

  // create imaage file from matrix
  gridToImageFile(programName, h_current_grid, 0, nrows, ncolumns, WITH_BORDER,
                  thread_count);

  err = cudaMemcpy(d_current_grid, h_current_grid,
                   nrows * ncolumns * sizeof(int), cudaMemcpyHostToDevice);
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    cudaFree(d_next_grid);
    printf("ERROR!: cudaMemcpy d_current_grid \n");
    exit(1);
  }

  err = cudaMemcpy(d_next_grid, h_current_grid, nrows * ncolumns * sizeof(int),
                   cudaMemcpyHostToDevice);
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    cudaFree(d_next_grid);
    printf("ERROR!: cudaMemcpy d_next_grid \n");
    exit(1);
  }

  // Parallel CUDA
  for (int currentStep = 1; currentStep <= nsteps; currentStep++) {

    // Kernel launch
    computeNextGridKernel<<<grid, block>>>(d_current_grid, d_next_grid, nrows,
                                           ncolumns);

    if (currentStep % IMAGE_INTERVAL == 0) {

      err = cudaMemcpy(h_current_grid, d_next_grid,
                       nrows * ncolumns * sizeof(int), cudaMemcpyDeviceToHost);
      if (err != cudaSuccess) {
        cudaFree(d_current_grid);
        cudaFree(d_next_grid);
        printf("ERROR!: cudaMemcpy h_current_grid : %s \n",
               cudaGetErrorString(err));
        exit(1);
      }

      // gridToCSVFile(programName, h_current_grid, currentStep, nrows,
      // ncolumns,
      //               WITH_BORDER); //TODO COMMENT
      gridToImageFile(programName, h_current_grid, currentStep, nrows, ncolumns,
                      WITH_BORDER, thread_count);
    }

    int *temp = d_current_grid;
    d_current_grid = d_next_grid;
    d_next_grid = temp;
  }

  // Iterations are done; sum the number of live cells
  int isum = 0;
#pragma omp parallel for num_threads(thread_count)                             \
    reduction(+ : isum) schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = gridTopRow; i <= gridBottomRow; i++) {
    for (int j = gridLeftColumn; j <= gridRightColumn; j++) {
      int index = i * ncolumns + j;
      isum += h_current_grid[index];
    }
  }

  printf("\nNumber of live cells = %d\n", isum);

  cudaFree(d_current_grid);
  cudaFree(d_next_grid);

  free(h_current_grid);

  gettimeofday(&stopTime, 0);
  // elapsedTime in milliseconds
  elapsedTimeMilliSec = (stopTime.tv_sec - startTime.tv_sec) * 1000.0f +
                        (stopTime.tv_usec - startTime.tv_usec) / 1000.0f;
  elapsedTimeSec = elapsedTimeMilliSec / 1000.0f;
  printf("Code executed with matrix size = %d x %d in: %.3f seconds = %.3f "
         "milliseconds.\n",
         initRows, initColumns, elapsedTimeSec, elapsedTimeMilliSec);

  return 0;
}
