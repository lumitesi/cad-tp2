/**
* CAD 2015/2016 - TP2
*
* Luis Silva,     nr 44890
* Ricardo Gaspar, nr 42038
*
* Conway's Game of Life
* Parallel Version using OpenMP - Version 1:
* The cells in the border of the grid are always considered to be dead.
*/

#include "file2ppm.c"
#include "sched.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

// Grid size
#define DEFAULT_NROWS 2048
#define DEFAULT_NCOLUMNS 2048
// number of time steps
#define DEFAULT_NSTEPS 100
// Generate image every IMAGE_INTERVAL
#define IMAGE_INTERVAL 10

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

enum CELL_STATES { DEAD = 0, ALIVE };

int thread_count = 1;

char *programName;

void clearBorder(int *grid, int nrows, int ncolumns) {
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  // left-right border
  for (int i = 0; i < nrows; i++) {
    int index1 = i * ncolumns + 0;
    int index2 = i * ncolumns + endColumn;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }

  // top-bottom border
  for (int j = 0; j < ncolumns; j++) {
    int index1 = 0 * ncolumns + j;
    int index2 = endRow * ncolumns + j;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }
}

/*  randomly generate values DEAD(0) or ALIVE (1) to each cell of the grid */
void generateGrid(int *grid, int nrows, int ncolumns) {
  clearBorder(grid, nrows, ncolumns);
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  srand(time(0)); // use current time as seed for random generator
  for (int i = 1; i < endRow; i++) {
    for (int j = 1; j < endColumn; j++) {
      int index = i * ncolumns + j;
      grid[index] = rand() % 2;
      // printf("Random value on [0,%d]: %d\n", RAND_MAX, grid[i][j]); //TODO
      // DEBUG
    }
  }
}

void computeNextGrid(int *current_grid, int *next_grid, int startRow,
                     int endRow, int startColumn, int endColumn, int nrows,
                     int ncolumns) {
  // For each cell compute its next life state (DEAD/ALIVE) based on its
  // neighbours
  // int up, down, left, right, nsum;
  int yUp, yDown, yUpLeft, yUpRight, yDownLeft, yDownRight, xLeft, xRight, nsum;
#pragma omp parallel for num_threads(thread_count) private(                    \
    yUp, yDown, yUpLeft, yUpRight, yDownLeft, yDownRight, xLeft, xRight, nsum) \
        schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = startRow; i <= endRow; i++) {
    for (int j = startColumn; j <= endColumn; j++) {

      int index = i * ncolumns + j;

      yUp = index - nrows;
      yDown = index + nrows;
      yUpLeft = yUp - 1;
      yUpRight = yUp + 1;
      yDownLeft = yDown - 1;
      yDownRight = yDown + 1;
      xLeft = index - 1;
      xRight = index + 1;

      nsum = current_grid[yUpLeft] + current_grid[yUp] +
             current_grid[yUpRight] + current_grid[xLeft] +
             current_grid[xRight] + current_grid[yDownLeft] +
             current_grid[yDown] + current_grid[yDownRight];

      switch (nsum) {
      case 3:
        next_grid[index] = ALIVE;
        break;

      case 2:
        next_grid[index] = current_grid[index];
        break;

      default:
        next_grid[index] = DEAD;
      }
    }
  }
}

int main(int argc, char *argv[]) {

  struct timeval startTime;
  struct timeval stopTime;
  float elapsedTimeMilliSec, elapsedTimeSec;

  if (argc < 5) {
    printf("Not enough arguments! %d\n", argc);
    printf(
        "Usage: %s numberOfGridRows numberOfGridColumns "
        "numberOfSimulationSteps numberOfThreads [initialGridStateFile.csv]\n",
        argv[0]);
    return 0;
  }

  programName = argv[0];
  int initRows = atoi(argv[1]);
  int initColumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  thread_count = atoi(argv[4]);

  int nrows, ncolumns;
  int *current_grid, *next_grid;

  gettimeofday(&startTime, 0);

  // allocate arrays
  nrows = initRows;       // add 2 for left and right ghost cells
  ncolumns = initColumns; // add 2 for top and bottom ghost cells
  int gridTopRow = 1, gridBottomRow = nrows - 2;
  int gridLeftColumn = 1, gridRightColumn = ncolumns - 2;
  current_grid = malloc(nrows * ncolumns * sizeof(int));
  next_grid = malloc(nrows * ncolumns * sizeof(int));

  // pupulate initial grid
  if (argc == 6) {
    char *initialGridFileName = argv[5];
    CSVfileToGrid(initialGridFileName, current_grid, nrows, ncolumns,
                  WITH_BORDER);
    clearBorder(current_grid, nrows, ncolumns);
  } else {
    generateGrid(current_grid, nrows, ncolumns);
  }
  clearBorder(next_grid, nrows, ncolumns);

  // create matrix file in CSV or Matrix
  // gridToCSVFile(programName, current_grid, 0, nrows, ncolumns, WITH_BORDER);

  // create image file from matrix
  gridToImageFile(programName, current_grid, 0, nrows, ncolumns, WITH_BORDER,
                  thread_count);

  //  time steps
  for (int currentStep = 1; currentStep <= nsteps; currentStep++) {
    computeNextGrid(current_grid, next_grid, gridTopRow, gridBottomRow,
                    gridLeftColumn, gridRightColumn, nrows, ncolumns);

    if (currentStep % IMAGE_INTERVAL == 0) {
      // create matrix file in CSV or Matrix
      // gridToCSVFile(programName, next_grid, currentStep, nrows, ncolumns,
      //               WITH_BORDER);
      // create image file
      gridToImageFile(programName, next_grid, currentStep, ncolumns, nrows,
                      WITH_BORDER, thread_count);
    }

    // copy next_grid state into current_grid state
    int *temp = current_grid;
    current_grid = next_grid;
    next_grid = temp;
  }

  // Iterations are done; sum the number of live cells
  int isum = 0;
#pragma omp parallel for num_threads(thread_count)                             \
    reduction(+ : isum) schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = gridTopRow; i <= gridBottomRow; i++) {
    for (int j = gridLeftColumn; j <= gridRightColumn; j++) {
      int index = i * ncolumns + j;
      isum += current_grid[index];
    }
  }

  printf("\nNumber of live cells = %d\n", isum);

  free(current_grid);
  free(next_grid);

  gettimeofday(&stopTime, 0);
  // elapsedTime in milliseconds
  elapsedTimeMilliSec = (stopTime.tv_sec - startTime.tv_sec) * 1000.0f +
                        (stopTime.tv_usec - startTime.tv_usec) / 1000.0f;
  elapsedTimeSec = elapsedTimeMilliSec / 1000.0f;
  printf("Code executed with matrix size = %d x %d in: %.3f seconds = %.3f "
         "milliseconds.\n",
         initRows, initColumns, elapsedTimeSec, elapsedTimeMilliSec);

  return 0;
}
