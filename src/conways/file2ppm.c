#include "sched.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

//#define SCHED_TYPE static
//#define SCHED_CHUNK_SIZE 1 // 1,2,4,8,16

void gridToImageFile(char *f, int *grid, int n, int numlines, int linelen,
                     int with_border, int thread_count) {
  char imagesdir[128];
  sprintf(imagesdir, "%s-images", f);
  //  printf("imagedir is: %s\n", imagesdir); // TODO DEBUG
  // Creates a new folder for ppm files
  struct stat st = {0};
  if (stat(imagesdir, &st) == -1) {
    mkdir(imagesdir, 0700);
  }

  FILE *fp;
  char name[128];
  unsigned char *image;
  int *apt;

  int nrows, startRow, endRow;
  int ncolumns, startColumn, endColumn;
  // printf("image with_border: %d\n", with_border); // TODO DEBUG
  if (with_border) {
    nrows = numlines;
    startRow = 0;
    endRow = numlines;
    ncolumns = linelen;
    startColumn = 0;
    endColumn = linelen;

  } else {
    nrows = numlines - 2;
    startRow = 1;
    endRow = numlines - 1;
    ncolumns = linelen - 2;
    startColumn = 1;
    endColumn = linelen - 1;
  }

  // printf("image malloc: %ld\n",
  //       nrows * ncolumns * sizeof(unsigned char)); // TODO DEBUG
  image = (unsigned char *)malloc(nrows * ncolumns * sizeof(unsigned char));
  // printf("image malloc done \n"); // TODO DEBUG

  //  printf("name before is: %s", name); // TODO DEBUG
  sprintf(name, "%s.%03d.ppm", f, n);
  //  printf("name after is: %s", name); // TODO DEBUG
  char imagepath[512];
  sprintf(imagepath, "%s/%s", imagesdir, name);
  //  printf("imagepath is: %s", imagepath); // TODO DEBUG

  fp = fopen(imagepath, "w+");

  // Write P5 format file header
  fprintf(fp, "P5\n %d\n %d\n %d\n", nrows, ncolumns, 255);

  apt = grid;

  // printf("nrows is: %d, ncolumns is: %d\n", nrows, ncolumns); // TODO DEBUG

  /* grid_row and grid_column are here for the case when the elements to iterate
   * from the grid are a subset of the grid (grid without border from [1,1] to
   * [n-2,n-2])*/
  int grid_row, grid_column;
#pragma omp parallel for num_threads(thread_count) private(grid_column)        \
    schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (grid_row = startRow; grid_row < endRow; grid_row++) {
    for (grid_column = startColumn; grid_column < endColumn; grid_column++) {

      int index = grid_row * linelen + grid_column;
      //  printf("\n grid_row: %d, grid_column: %d, value: % d\n ", grid_row,
      //     grid_column,
      //   grid[index]); // TODO DEBUG
      switch (apt[index]) {
      case 0:
        image[index] = 0;
        break;
      case 1:
        image[index] = 255;
        break;
      }
    }

    // for (int row = 0; row < nrows; row++) {
    //  fwrite(image[row], sizeof(unsigned char), ncolumns, fp);
    //}
    // fwrite(image, 1, ncolumns * nrows * sizeof(unsigned char), fp); // TODO
  }
  fwrite(image, sizeof(unsigned char), ncolumns * nrows, fp);
  fclose(fp);
  free(image);
}

void gridToCSVFile(char *f, int *grid, int n, int numlines, int linelen,
                   int with_border) {

  char matricesdir[256];
  sprintf(matricesdir, "%s-matrices", f);
  // Creates a new folder for matrix files
  struct stat st = {0};
  if (stat(matricesdir, &st) == -1) {
    mkdir(matricesdir, 0700);
  }

  char name[128];
  FILE *fout;

  sprintf(name, "%s.%03d", f, n);
  char matricespath[512];
  sprintf(matricespath, "%s/%s.csv", matricesdir, name);

  fout = fopen(matricespath, "w+");

  int startRow, endRow;
  int startColumn, endColumn;
  // printf("gridToCSVFile file %d\n", n); // TODO DEBUG
  if (with_border) {
    // printf("with border!\n"); // TODO DEBUG
    startRow = 0;
    endRow = numlines;
    startColumn = 0;
    endColumn = linelen;
  } else {
    startRow = 1;
    endRow = numlines - 1;
    startColumn = 1;
    endColumn = linelen - 1;
  }
  // printf("with_border: %d, startRow = %d, startColumn = %d, endRow = %d, "
  //        "endColumn = %d\n",
  //        with_border, startRow, startColumn, endRow, endColumn); // TODO
  //        DEBUG
  int numberOfLines = (endColumn - startColumn);
  char line[3 * numberOfLines];
  for (int i = startRow; i < endRow; ++i) {
    line[0] = '\0';
    for (int j = startColumn; j < endColumn; ++j) {
      int index = i * linelen + j;
      // printf("\ni is: %d, j is: %d, value: %d\n", i, j,
      //        grid[i][j]); // TODO DEBUG
      if (grid[index] == 0) {
        strcat(line, "0,");
        // fprintf(fout, "0,");
      } else {
        // fprintf(fout, "1,");
        strcat(line, "1,");
      }
    }
    // printf("line-%d:%s\n", i, line); // TODO DEBUG
    fprintf(fout, "%s\n", line);
    // printf("line-%d:%s\n", i, line); // TODO DEBUG
    // fprintf(fout, "\n");
  }
  fclose(fout);
}

/*
* Reads a matrix in a CSV file to a grid. The nrows and ncolumns refer to the
* size of the grid argument. When atepmting to read from a file be aware of the
* matrix size. If with_border = 1, the matrix in the file has the same size has
* the grid and it will be written from position [0,0] to [nrows-1,ncolumns-1].
* Otherwise, the grid is bigger and the values in the file will be written
* starting from position [1,1] to [nrows-2][ncolumns-2].
*/
void CSVfileToGrid(char *name, int *grid, int nrows, int ncolumns,
                   int with_border) {
  FILE *fint;
  fint = fopen(name, "r");
  int startRow, endRow;
  int startColumn, endColumn;

  if (with_border) {
    // printf("with border!\n"); // TODO DEBUG
    startRow = 0;
    endRow = nrows;
    startColumn = 0;
    endColumn = ncolumns;
  } else {
    startRow = 1;
    endRow = nrows - 1;
    startColumn = 1;
    endColumn = ncolumns - 1;
  }

  char line[ncolumns * 3];
  for (int i = startRow; i < endRow; i++) {
    fgets(line, ncolumns * 3, fint);
    // printf("row:%d cannot read line NULL\n", i); // TODO DEBUG
    // printf("\nread line %d: %s.\n", i, line); // TODO DEBUG

    // parsing line
    char *token = strtok(line, ",");
    for (int j = startColumn; j < endColumn; j++) {
      int index = i * ncolumns + j;
      // printf("token %d: %s.\n", j, token); // TODO DEBUG
      grid[index] = atoi(token);
      // printf("grid[%d][%d]:%d.\n", i, j, grid[index]); // TODO DEBUG
      token = strtok(NULL, ",");
    }
  }
  fclose(fint);
}
