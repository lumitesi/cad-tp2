/**
* CAD 2015/2016 - TP2
*
* Luis Silva,     nr 44890
* Ricardo Gaspar, nr 42038
*
* Conway's Game of Life
* Parallel Version using CUDA C - Version 2:
* The cells in the border of the grid are always considered to be dead.
* Grids don't fit in memory.
*/

#include "file2ppm.c"
#include "sched.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

// Grid size
#define DEFAULT_NROWS 2048
#define DEFAULT_NCOLUMNS 2048
// number of time steps
#define DEFAULT_NSTEPS 100
// Generate image every IMAGE_INTERVAL
#define IMAGE_INTERVAL 10
// Theses constants define the number of rows and columns used for CUDA grid and
// blocks.
#define ROW_BLOCKS_PER_GRID 2
#define ROW_THREADS_PER_BLOCK 8

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

enum CELL_STATES { DEAD = 0, ALIVE };

int thread_count = 1;

char *programName;

void clearBorder(int *grid, int nrows, int ncolumns) {
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  // left-right border
  for (int i = 0; i < nrows; i++) {
    int index1 = i * ncolumns + 0;
    int index2 = i * ncolumns + endColumn;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }

  // top-bottom border
  for (int j = 0; j < ncolumns; j++) {
    int index1 = 0 * ncolumns + j;
    int index2 = endRow * ncolumns + j;
    grid[index1] = DEAD;
    grid[index2] = DEAD;
  }
}

/*  randomly generate values DEAD(0) or ALIVE (1) to each cell of the grid */
void generateGrid(int *grid, int nrows, int ncolumns) {
  clearBorder(grid, nrows, ncolumns);
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  srand(time(0)); // use current time as seed for random generator
  for (int i = 1; i < endRow; i++) {
    for (int j = 1; j < endColumn; j++) {
      int index = i * ncolumns + j;
      grid[index] = rand() % 2;
      // printf("Random value on [0,%d]: %d\n", RAND_MAX, grid[i][j]); //TODO
    }
  }
}

__global__ void computeNextGridKernel(int *d_current_grid, int *d_next_grid,
                                      int nrows, int ncolumns) {
  int matrixSize = nrows * ncolumns;

  // The for cycle starts at (blockIdx.x * blockDim.x + threadIdx.x) + ncolumns
  // to jump the first row of the grid which is the border, also avoids the last
  // row matrixSize - ncolumns
  for (int index = (blockIdx.x * blockDim.x + threadIdx.x) + ncolumns;
       index < matrixSize - ncolumns; index += blockDim.x * gridDim.x) {
    int x = index % ncolumns;

    // Ignore the border columns
    if (x != 0 && x != ncolumns - 1) {
      int yUp = index - ncolumns;
      int yDown = index + ncolumns;
      int yUpLeft = yUp - 1;
      int yUpRight = yUp + 1;
      int yDownLeft = yDown - 1;
      int yDownRight = yDown + 1;
      int xLeft = index - 1;
      int xRight = index + 1;

      int nsum = d_current_grid[yUpLeft] + d_current_grid[yUp] +
                 d_current_grid[yUpRight] + d_current_grid[xLeft] +
                 d_current_grid[xRight] + d_current_grid[yDownLeft] +
                 d_current_grid[yDown] + d_current_grid[yDownRight];

      switch (nsum) {
      case 3:
        d_next_grid[index] = ALIVE;
        break;

      case 2:
        d_next_grid[index] = d_current_grid[index];
        break;

      default:
        d_next_grid[index] = DEAD;
      }
    }
  }
}

/**
* Copys the specified portion of the original grid to the given partition grid.
**/
void copyGridToPartition(int *current_grid, int nrows, int ncolumns,
                         int startRow, int startColumn, int endRow,
                         int endColumn, int *partition_grid, int partitionRows,
                         int partitionColumns) {
  for (int current_gridi = startRow, i = 0; i <= partitionRows - 1;
       current_gridi++, i++) {
    for (int current_gridj = startColumn, j = 0; j <= partitionColumns - 1;
         current_gridj++, j++) {
      int partIndex = i * partitionColumns + j;
      int currIndex = current_gridi * ncolumns + current_gridj;

      partition_grid[partIndex] = current_grid[currIndex];
    }
  }
}

/**
* Copys the specified portion of the original grid to the given partition grid.
**/
void copyPartitionToGrid(int *next_grid, int nrows, int ncolumns, int startRow,
                         int startColumn, int endRow, int endColumn,
                         int *partition_grid, int partitionRows,
                         int partitionColumns, int includeTopRow,
                         int includeBottomRow, int includeLeftColumn,
                         int includeRightColumn) {

  int partition_startRow = 0, partition_endRow = partitionRows - 1;
  int partition_startColumn = 0, partition_endColumn = partitionColumns - 1;

  // outer rows & columns previously included in the partition necessary to
  // kernel to compute the partition. To copy back to grid they need to be
  // ignored.
  if (includeTopRow) {
    partition_startRow++;
  }
  if (includeBottomRow) {
    partition_endRow--;
  }
  if (includeLeftColumn) {
    partition_startColumn++;
  }
  if (includeRightColumn) {
    partition_endColumn--;
  }

  for (int next_gridi = startRow, i = partition_startRow; i <= partition_endRow;
       next_gridi++, i++) {
    for (int next_gridj = startColumn, j = partition_startColumn;
         j <= partition_endColumn; next_gridj++, j++) {

      int partIndex = i * partitionColumns + j;
      int nextIndex = next_gridi * ncolumns + next_gridj;

      next_grid[nextIndex] = partition_grid[partIndex];
    }
  }
}

/* Allocates two grids in the device with the given size (nrows*ncolumns) */
void deviceMallocGrids(int **d_current_grid, int **d_next_grid, int nrows,
                       int ncolumns) {

  // printf("\n[deviceMallocGrids] d_current_grid value: %ld, address: %ld, "
  //        "deref: %ld\n",
  //        d_current_grid, &d_current_grid, *d_current_grid); // TODO DEBUG
  // printf(
  //     "[deviceMallocGrids] d_next_grid value: %ld, address: %ld, deref:
  //     %ld\n",
  //     d_next_grid, &d_next_grid, *d_next_grid); // TODO DEBUG

  cudaError_t err;
  err = cudaMalloc((void **)d_current_grid, nrows * ncolumns * sizeof(int));
  if (err != cudaSuccess) {
    printf("ERROR!: cudaMalloc d_current_grid: %s\n", cudaGetErrorString(err));
    exit(1);
  }

  err = cudaMalloc((void **)d_next_grid, nrows * ncolumns * sizeof(int));
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    printf("ERROR!: cudaMalloc d_next_grid: %s\n", cudaGetErrorString(err));
    exit(1);
  }
}

/* Frees two grids in the device */
void deviceFreeGrids(int *d_current_grid, int *d_next_grid) {
  cudaFree(d_current_grid);
  cudaFree(d_next_grid);
}

/* Initalizes the two grids in the device with the given portion of the original
 * grid. This is done in order to fill the same border for both grids. This way
 * the d_next_grid can be computed by the kernel which only computes the inside
 * of this grid.*/
void copyGridsToDevice(int *partition_grid, int *d_current_grid,
                       int *d_next_grid, int partitionRows,
                       int partitionColumns) {

  // printf("\n[copyGridsToDevice] partition_grid value: %ld, address: %ld, "
  //        "deref: %ld\n",
  //        partition_grid, &partition_grid, *partition_grid); // TODO DEBUG

  // printf("[copyGridsToDevice] d_current_grid value: %ld, address: %ld"
  //        "%ld\n",
  //        d_current_grid, &d_current_grid); // TODO DEBUG

  // printf("[copyGridsToDevice] d_next_grid value: %ld, address: %ld\n",
  //        d_next_grid, &d_next_grid); // TODO DEBUG

  cudaError_t err;
  err = cudaMemcpy(d_current_grid, partition_grid,
                   partitionRows * partitionColumns * sizeof(int),
                   cudaMemcpyHostToDevice);
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    cudaFree(d_next_grid);
    printf("ERROR!: cudaMemcpy d_current_grid : %s\n", cudaGetErrorString(err));
    exit(1);
  }

  err = cudaMemcpy(d_next_grid, d_current_grid,
                   partitionRows * partitionColumns * sizeof(int),
                   cudaMemcpyDeviceToDevice);
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    cudaFree(d_next_grid);
    printf("ERROR!: cudaMemcpy d_next_grid %s\n", cudaGetErrorString(err));
    exit(1);
  }
}

void copyGridToHost(int *partition_grid, int *d_current_grid, int *d_next_grid,
                    int nrows, int ncolumns) {

  // printf("\n[copyGridToHost] partition_grid value: %ld, address: %ld, "
  //        "deref: %ld\n",
  //        partition_grid, &partition_grid, *partition_grid); // TODO DEBUG

  // printf("[copyGridToHost] d_current_grid value: %ld, address: %ld"
  //        "%ld\n",
  //        d_current_grid, &d_current_grid); // TODO DEBUG

  // printf("[copyGridToHost] d_next_grid value: %ld, address: %ld\n",
  // d_next_grid,
  //        &d_next_grid); // TODO DEBUG

  cudaError_t err;
  err = cudaMemcpy(partition_grid, d_next_grid, nrows * ncolumns * sizeof(int),
                   cudaMemcpyDeviceToHost);
  if (err != cudaSuccess) {
    cudaFree(d_current_grid);
    cudaFree(d_next_grid);
    printf("ERROR!: cudaMemcpy partition_grid : %s\n", cudaGetErrorString(err));
    exit(1);
  }
}

size_t getFreeBytes() {
  size_t free_bytes;
  size_t total_bytes;

  cudaError_t err = cudaMemGetInfo(&free_bytes, &total_bytes);
  if (err != cudaSuccess) {
    printf("Not enough available memory in GPU! %s\n", cudaGetErrorString(err));
    exit(1);
  }
  return free_bytes;
}

int main(int argc, char *argv[]) {

  struct timeval startTime;
  struct timeval stopTime;
  float elapsedTimeMilliSec, elapsedTimeSec;

  if (argc < 5) {
    printf("Not enough arguments! %d\n", argc);
    printf(
        "Usage: %s numberOfGridRows numberOfGridColumns "
        "numberOfSimulationSteps numberOfThreads [initialGridStateFile.csv]\n",
        argv[0]);
    return 0;
  }

  programName = argv[0];
  int initRows = atoi(argv[1]);
  int initColumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  thread_count = atoi(argv[4]);

  int nrows, ncolumns;

  /* Partitions grid is an abstraction derived from dividing the
   * h_current_grid into partitions with
   * partitionRowSize*partitionColumnSize */

  // number and columns of rows of a partition
  int partitionRowSize = DEFAULT_NROWS / 2;
  int partitionColumnSize = DEFAULT_NCOLUMNS / 2;

  if (initRows < partitionRowSize) {
    partitionRowSize = initRows;
  }
  if (initColumns < partitionColumnSize) {
    partitionColumnSize = initColumns;
  }

  // number of partitions (or divisions)
  int partitionRows = initRows / partitionRowSize;
  int partitionColumns = initColumns / partitionColumnSize;

  // printf("partitionRows = %d, partitionColumns = %d, partitionRowSize = %d, "
  //        "partitionColumnSize = %d\n",
  //        partitionRows, partitionColumns, partitionRowSize,
  //        partitionColumnSize); // TODO DEBUG

  // partition_grid represents a given partition (portion of the h_current_grid)
  int *partition_grid;
  int *h_current_grid, *h_next_grid;
  int *d_current_grid, *d_next_grid;

  // CUDA Initialization
  cudaError_t err;
  err = cudaSetDevice(0);
  if (err != cudaSuccess) {
    printf("ERROR!: cudaSetDevice: %s\n", cudaGetErrorString(err));
    exit(1);
  }

  // CUDA Check free memory
  long free_bytes;
  free_bytes = getFreeBytes();
  if (free_bytes < 3 * (initRows * initColumns * sizeof(int))) {
    printf("CUDA Not enough memory to run! \n");
    printf("Execution aborted. \n");
    exit(1);
  }
  printf("CUDA: FreeMem :  %ld\n", free_bytes);

  int grid = ROW_BLOCKS_PER_GRID * ROW_BLOCKS_PER_GRID;
  int block = ROW_THREADS_PER_BLOCK * ROW_THREADS_PER_BLOCK;

  // allocate arrays
  nrows = initRows;       // add 2 for left and right ghost cells
  ncolumns = initColumns; // add 2 for top and bottom ghost cells
  int gridTopRow = 1, gridBottomRow = nrows - 2;
  int gridLeftColumn = 1, gridRightColumn = ncolumns - 2;

  gettimeofday(&startTime, 0);

  h_current_grid = (int *)malloc(nrows * ncolumns * sizeof(int));
  h_next_grid = (int *)malloc(nrows * ncolumns * sizeof(int));

  // populate initial grid
  if (argc == 6) {
    char *initialGridFileName = argv[5];
    CSVfileToGrid(initialGridFileName, h_current_grid, nrows, ncolumns,
                  WITH_BORDER);
    clearBorder(h_current_grid, nrows, ncolumns);
  } else {
    generateGrid(h_current_grid, nrows, ncolumns);
  }

  // create matrix file in CSV or Matrix
  // gridToCSVFile(programName, h_current_grid, 0, nrows, ncolumns,
  //               WITH_BORDER); // TODO COMMENT

  // create image file from matrix
  gridToImageFile(programName, h_current_grid, 0, nrows, ncolumns, WITH_BORDER,
                  thread_count); // TODO UNCOMMENT

  // Parallel CUDA, compute each partition of the initial grid
  for (int currentStep = 1; currentStep <= nsteps; currentStep++) {
    // printf("[main] currentStep: %d\n", currentStep); // TODO DEBUG
    // compute partition
    for (int i = 0; i < partitionRows; i++) {
      for (int j = 0; j < partitionColumns; j++) {
        // printf("[main] partitionRow: %d partitionColumn: %d\n", i,j); // TODO
        // DEBUG

        // calculate indexes to use for the partition (read from h_current_grid
        // and write to h_next_grid)
        int current_startRow = i * partitionRowSize,
            next_startRow = current_startRow;
        int current_startColumn = j * partitionColumnSize,
            next_startColumn = current_startColumn;
        int current_endRow = current_startRow + partitionRowSize - 1,
            next_endRow = current_endRow;
        int current_endColumn = current_startColumn + partitionColumnSize - 1,
            next_endColumn = current_endColumn;

        /** To process the partion we need the surrounding rows/columns
         * depending on the partition (where it is located in the h_current_grid
         * is) */
        int deviceRows = partitionRowSize;
        int deviceColumns = partitionColumnSize;

        // Indicates the rows/columns to include from h_current_grid
        // 0 = false = don't include , >=1 = true = include
        int includeTopRow = 0, includeBottomRow = 0;
        int includeLeftColumn = 0, includeRightColumn = 0;

        // Partition is in the first row of partitions grid
        if (i == 0) {
          includeBottomRow = 1;
          current_endRow += 1;
          deviceRows++;
        }
        // Partition is in the last row of partitions grid
        else if (i == partitionRows - 1) {
          includeTopRow = 1;
          current_startRow -= 1;
          deviceRows++;
        }
        // Partition is in a middle row of partitions grid
        else {
          includeTopRow = 1;
          includeBottomRow = 1;
          current_startRow -= 1;
          current_endRow += 1;
          deviceRows += 2;
        }
        // Partition is in the first column of partitions grid
        if (j == 0) {
          includeRightColumn = 1;
          current_endColumn += 1;
          deviceColumns++;
        }
        // Partition is in the last column of partitions grid
        else if (j == partitionColumns - 1) {
          includeLeftColumn = 1;
          current_startColumn -= 1;
          deviceColumns++;
        }
        // Partition is in a middle column of partitions grid
        else {
          includeLeftColumn = 1;
          includeRightColumn = 1;
          current_startColumn -= 1;
          current_endColumn += 1;
          deviceColumns += 2;
        }

        // printf(
        //     "\n[main] BEFORE MALLOC partition_grid value: %ld, address:
        //     %ld\n",
        //     partition_grid,
        //     &partition_grid); // TODO DEBUG
        // printf("[main] BEFORE MALLOC d_current_grid value: %ld, address:
        // %ld\n",
        //        d_current_grid,
        //        &d_current_grid); // TODO DEBUG
        // printf("[main] BEFORE MALLOC d_next_grid value: %ld, address: %ld\n",
        //        d_next_grid,
        //        &d_next_grid); // TODO DEBUG

        partition_grid =
            (int *)malloc(deviceRows * deviceColumns * sizeof(int));

        copyGridToPartition(h_current_grid, nrows, ncolumns, current_startRow,
                            current_startColumn, current_endRow,
                            current_endColumn, partition_grid, deviceRows,
                            deviceColumns);

        // char partition_name[100]; // TODO DEBUG
        // sprintf(partition_name, "%s-step%d-read", programName,
        //         currentStep); // TODO DEBUG
        // printf(
        //     "partition_name: %s-r%d-c%d, deviceRows: %d, deviceColumns:
        //     %d\n",
        //     partition_name, i, j, deviceRows, deviceColumns); // TODO DEBUG
        // gridToCSVFile(partition_name, partition_grid, i * partitionColumns +
        // j,
        //               deviceRows, deviceColumns, WITH_BORDER); // TODO DEBUG

        deviceMallocGrids(&d_current_grid, &d_next_grid, deviceRows,
                          deviceColumns);

        // printf("\n[main] partition_grid value: %ld, address: %ld\n",
        //        partition_grid,
        //        &partition_grid); // TODO DEBUG
        // printf("[main] d_current_grid value: %ld, address: %ld\n",
        //        d_current_grid,
        //        &d_current_grid); // TODO DEBUG
        // printf("[main] d_next_grid value: %ld, address: %ld\n", d_next_grid,
        //        &d_next_grid); // TODO DEBUG

        copyGridsToDevice(partition_grid, d_current_grid, d_next_grid,
                          deviceRows, deviceColumns);

        // Kernel launch
        computeNextGridKernel<<<grid, block>>>(d_current_grid, d_next_grid,
                                               deviceRows, deviceColumns);

        copyGridToHost(partition_grid, d_current_grid, d_next_grid, deviceRows,
                       deviceColumns);

        // printf("[main] copyGridToHost completed!\n"); // TODO DEBUG

        // sprintf(partition_name, "%s-step%d-write", programName,
        //         currentStep); // TODO DEBUG
        // printf(
        //     "partition_name: %s-r%d-c%d, deviceRows: %d, deviceColumns:
        //     %d\n",
        //     partition_name, i, j, deviceRows, deviceColumns); // TODO DEBUG
        // gridToCSVFile(partition_name, partition_grid, i * partitionColumns +
        // j,
        //               deviceRows, deviceColumns, WITH_BORDER); // TODO DEBUG

        copyPartitionToGrid(h_next_grid, nrows, ncolumns, next_startRow,
                            next_startColumn, next_endRow, next_endColumn,
                            partition_grid, deviceRows, deviceColumns,
                            includeTopRow, includeBottomRow, includeLeftColumn,
                            includeRightColumn);
        // printf("[main] copyPartitionToGrid completed!\n"); // TODO DEBUG

        free(partition_grid);
        deviceFreeGrids(d_current_grid, d_next_grid);
      }
    }

    if (currentStep % IMAGE_INTERVAL == 0) {

      // gridToCSVFile(programName, h_next_grid, currentStep, nrows, ncolumns,
      //               WITH_BORDER); // TODO COMMENT
      gridToImageFile(programName, h_next_grid, currentStep, nrows, ncolumns,
                      WITH_BORDER, thread_count); // TODO UNCOMMENT
    }

    int *temp = h_current_grid;
    h_current_grid = h_next_grid;
    h_next_grid = temp;
  }

  // Iterations are done; sum the number of live cells
  int isum = 0;
#pragma omp parallel for num_threads(thread_count)                             \
    reduction(+ : isum) schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = gridTopRow; i <= gridBottomRow; i++) {
    for (int j = gridLeftColumn; j <= gridRightColumn; j++) {
      int index = i * ncolumns + j;
      isum += h_current_grid[index];
    }
  }

  printf("\nNumber of live cells = %d\n", isum);

  cudaFree(d_current_grid);
  cudaFree(d_next_grid);

  free(h_current_grid);

  gettimeofday(&stopTime, 0);
  // elapsedTime in milliseconds
  elapsedTimeMilliSec = (stopTime.tv_sec - startTime.tv_sec) * 1000.0f +
                        (stopTime.tv_usec - startTime.tv_usec) / 1000.0f;
  elapsedTimeSec = elapsedTimeMilliSec / 1000.0f;
  printf("Code executed with matrix size = %d x %d in: %.3f seconds = %.3f "
         "milliseconds.\n",
         initRows, initColumns, elapsedTimeSec, elapsedTimeMilliSec);

  return 0;
}
