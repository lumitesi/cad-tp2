#!/bin/bash

echo Started...

script_name="run_cuda_v1_2048_500g_16th_5runs"

## Current Directory - Scripts folder
current_dir=$PWD

path_conways="../conways/"
exe="gameoflife_cuda_v1"

matrix_size="2048"
n_gen="500"
n_threads="16"
n_runs=5

csv="csv-2048-300-264446.csv"
csv_path="../input_Matrix/$csv"

ARGS=" $matrix_size $matrix_size $n_gen $n_threads $csv_path"


output_file="$current_dir/$script_name-results.txt"

## Output File Header
echo "$script_name" >> $output_file
echo "" >> $output_file
echo "Matrix size: $matrix_size x $matrix_size" >> $output_file
echo "Generations: $n_gen" >> $output_file
echo "Threads: $n_threads" >> $output_file
echo "Runs: $n_runs" >> $output_file
echo "" >> $output_file

#echo "current dir: " $current_dir;

# Change directory to the executable folder
cd $path_conways;

# Cleans all files from previous runs
make clean;
rm -r "$exe-images"
rm -r "$exe-matrices"
echo "PPM and Matrix folders removed"

# Compiles
make $exe;

# Exec's the program n_runs times
for (( i=1; i <= $n_runs; ++i ))
do
    echo "Running test: $i"
    echo "" >> $output_file;
    echo "Running test: $i" >> $output_file;
    ./$exe $ARGS >> $output_file;
done

cd "$current_dir";

echo "done!"
