# TP2_CAD_CUDA
## Conway's Game of Life

## ToCompile
Existe um makefile na pasta src/conways, bastando usar make seguido do nome da versão.

Ex.
```bash
make gameoflife_cuda_v1
```

## Run
```bash
./gameoflife_cuda_v1 nRows nColumms nGenerations nThreads [input.csv]```

Ainda assim estão disponiveis vários scropts, na pasta src/scripts, que foram utilizados para correr os testes às várias versões do programa.
