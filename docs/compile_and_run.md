# CAD Compile and Run

## Conway's Game of Life Sequential

Commands to be run on the version folder.
#### Compile
```gcc -g -Wall -o gameoflife gameoflife.c
```

#### Run

```sh 
./gameoflife rows cols nGenerations seedFile outputName
```

or -> Generate Random seed file

```sh 
./gameoflife rows cols outputName
```

###### Exemple
```sh
./gameoflife 256 256 in256
```

```sh
./gameoflife 256 256 300 in256_000.matrix out256
```

###### Convert to Gif (ImageMagik)
```sh
convert out256/out256_*.ppm out256/out256.gif
```

## Conway's Game of Life V1

#### Compile
```gcc -g -Wall -fopenmp -o gameoflife gameoflife.c
```

#### Run

```sh 
./gameoflife rows cols num_Generations seedFile outputName num_Threads
```
###### Exemple
```sh
./gameoflife 256 256 in256
```

```sh
./gameoflife 256 256 300 in256_000.matrix out256 4
```
###### Static Schedule
```gcc -g -Wall -fopenmp -o gameoflife_static gameoflife_static.c
```

```sh
./gameoflife_static 256 256 300 in256_000.matrix out256 4
```

###### Dynamic Schedule

```gcc -g -Wall -fopenmp -o gameoflife_dynamic gameoflife_dynamic.c
```

```sh
./gameoflife_dynamic 256 256 300 in256_000.matrix out256 4
```

## Conway's Game of Life V2

#### Compile
```gcc -g -Wall -fopenmp -o gameoflife gameoflife.c
```

#### Run

```sh 
./gameoflife rows cols num_Generations seedFile outputName num_Threads
```
###### Exemple
```sh
./gameoflife 256 256 in256
```

```sh
./gameoflife 256 256 300 in256_000.matrix out256 4
```
###### Static Schedule
```gcc -g -Wall -fopenmp -o gameoflife_static gameoflife_static.c
```

```sh
./gameoflife_static 256 256 300 in256_000.matrix out256 4
```

###### Dynamic Schedule

```gcc -g -Wall -fopenmp -o gameoflife_dynamic gameoflife_dynamic.c
```

```sh
./gameoflife_dynamic 256 256 300 in256_000.matrix out256 4
```