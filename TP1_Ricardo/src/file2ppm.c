#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

void gridToImageFile(char *f, int **grid, int n, int numlines, int linelen,
                     int with_border) {
  char imagesdir[60];
  sprintf(imagesdir, "%s-images", f);
  // printf("imagedir is: %s", imagesdir); // TODO DEBUG
  if (n == 0) {
    mkdir(imagesdir, S_IRWXU);
  }

  FILE *fp;
  char name[32];
  unsigned char *image;
  int **apt;

  int nrows, startRow, endRow;
  int ncolumns, startColumn, endColumn;
  // printf("image with_border: %d\n", with_border); // TODO DEBUG
  if (with_border) {
    nrows = numlines;
    startRow = 0;
    endRow = numlines;
    ncolumns = linelen;
    startColumn = 0;
    endColumn = linelen;

  } else {
    nrows = numlines - 2;
    startRow = 1;
    endRow = numlines - 1;
    ncolumns = linelen - 2;
    startColumn = 1;
    endColumn = linelen - 1;
  }

  image = calloc(ncolumns * nrows * 3, sizeof(unsigned char));

  sprintf(name, "%s.%03d", f, n);
  char imagepath[256];
  sprintf(imagepath, "%s/%s", imagesdir, name);
  // printf("imagepath is: %s", imagepath); // TODO DEBUG

  fp = fopen(imagepath, "w+");
  /* Print the P6 format header */
  fprintf(fp, "P6\n%d %d\n255\n", ncolumns, nrows);

  apt = grid;

  // printf("nrows is: %d, ncolumns is: %d\n", nrows, ncolumns); // TODO DEBUG

  /* grid_row and grid_column are here for the case when the elements to iterate
   * from the grid are a subset of the grid (grid without border from [1,1] to
   * [n-2,n-2])*/
  for (int grid_row = startRow, i = 0; grid_row < endRow; grid_row++, i++)
    for (int grid_column = startColumn, j = 0; grid_column < endColumn;
         grid_column++, j++) {
      // printf("\ni is: %d, j is: %d, grid_row: %d, grid_column: %d, value:
      // %d\n",
      //        i, j, grid_row, grid_column,
      //        grid[grid_row][grid_column]); // TODO DEBUG
      switch (apt[grid_row][grid_column]) {
      case 0:
        image[(ncolumns * i + j) * 3] = 0;
        image[(ncolumns * i + j) * 3 + 1] = 250;
        image[(ncolumns * i + j) * 3 + 2] = 0;
        break;
      case 1:
        image[(ncolumns * i + j) * 3] = 250;
        image[(ncolumns * i + j) * 3 + 1] = 0;
        image[(ncolumns * i + j) * 3 + 2] = 0;
        break;
      case 2:
        image[(ncolumns * i + j) * 3] = 0;
        image[(ncolumns * i + j) * 3 + 1] = 0;
        image[(ncolumns * i + j) * 3 + 2] = 0;
      }
    }

  fwrite(image, 1, ncolumns * nrows * 3, fp);
  fclose(fp);
  free(image);
}

void gridToFile(char *f, int **grid, int n, int numlines, int linelen,
                int with_border) {
  char matricesdir[60];
  sprintf(matricesdir, "%s-matrices", f);
  if (n == 0) {
    mkdir(matricesdir, S_IRWXU);
  }

  char name[32];
  FILE *fout;

  sprintf(name, "%s.%03d", f, n);
  char matricespath[256];
  sprintf(matricespath, "%s/%s.csv", matricesdir, name);

  fout = fopen(matricespath, "w+");

  int startRow, endRow;
  int startColumn, endColumn;
  // printf("with_border: %d\n", with_border); // TODO DEBUG
  // printf("gridToFile file %d\n", n); // TODO DEBUG
  if (with_border) {
    // printf("with border!\n"); // TODO DEBUG
    startRow = 0;
    endRow = numlines;
    startColumn = 0;
    endColumn = linelen;
  } else {
    startRow = 1;
    endRow = numlines - 1;
    startColumn = 1;
    endColumn = linelen - 1;
  }
  int numberOfLines = (endColumn - startColumn);
  char line[3 * numberOfLines];
  for (int i = startRow; i < endRow; ++i) {
    line[0] = '\0';
    for (int j = startColumn; j < endColumn; ++j) {
      // printf("\ni is: %d, j is: %d, value: %d\n", i, j,
      //        grid[i][j]); // TODO DEBUG
      if (grid[i][j] == 0) {
        strcat(line, "0,");
        // fprintf(fout, "0,");
      } else {
        // fprintf(fout, "1,");
        strcat(line, "1,");
      }
    }
    // printf("line-%d:%s\n", i, line); // TODO DEBUG
    fprintf(fout, "%s\n", line);
    // printf("line-%d:%s\n", i, line); // TODO DEBUG
    // fprintf(fout, "\n");
  }
  fclose(fout);
}

/*
* Reads a matrix in a CSV file to a grid. The nrows and ncolumns refer to the
* size of the grid argument. When atepmting to read from a file be aware of the
* matrix size. If with_border = 1, the matrix in the file has the same size has
* the grid and it will be written from position [0,0] to [nrows-1,ncolumns-1].
* Otherwise, the grid is bigger and the values in the file will be written
* starting from position [1,1] to [nrows-2][ncolumns-2].
*/
void fileToGrid(char *name, int **grid, int nrows, int ncolumns,
                int with_border) {
  FILE *fint;
  fint = fopen(name, "r");
  int startRow, endRow;
  int startColumn, endColumn;

  if (with_border) {
    // printf("with border!\n"); // TODO DEBUG
    startRow = 0;
    endRow = nrows;
    startColumn = 0;
    endColumn = ncolumns;
  } else {
    startRow = 1;
    endRow = nrows - 1;
    startColumn = 1;
    endColumn = ncolumns - 1;
  }

  char line[ncolumns * 3];
  for (int i = startRow; i < endRow; i++) {
    fgets(line, ncolumns * 3, fint);
    // printf("row:%d cannot read line NULL\n", i); //TODO DEBUG
    // printf("\nread line %d: %s.\n", i, line); // TODO DEBUG

    // parsing line
    char *token = strtok(line, ",");
    for (int j = startColumn; j < endColumn; j++) {
      // printf("token %d: %s.\n", j, token); // TODO DEBUG
      grid[i][j] = atoi(token);
      // printf("grid[%d][%d]:%d.\n", i, j, grid[i][j]); // TODO DEBUG
      token = strtok(NULL, ",");
    }
  }
  fclose(fint);
}
