/**
* CAD 2015/2016 - TP1
* Ricardo Gaspar, nr 42038
* Conway's Game of Life
* Parallel Version with static schedulling - Version 1:
* The cells in the border of the grid are always considered to be dead.
*/

#include "file2ppm.c"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define PROGRAM_NAME "cglife-v1-par-guided-sched"

// Grid size
#define DEFAULT_NROWS 2048
#define DEFAULT_NCOLUMNS 2048
// number of time steps
#define DEFAULT_NSTEPS 100

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

#define SCHED_TYPE guided
#define SCHED_CHUNK_SIZE 16 // 1,2,4,8,16

enum CELL_STATES { DEAD = 0, ALIVE };

int thread_count = 1;

void clearBorder(int **grid, int nrows, int ncolumns) {
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  // left-right border
  for (int i = 0; i < nrows; i++) {
    grid[i][0] = DEAD;
    grid[i][endColumn] = DEAD;
  }

  // top-bottom border
  for (int j = 0; j < ncolumns; j++) {
    grid[0][j] = DEAD;
    grid[endRow][j] = DEAD;
  }
}

/*  randomly generate values DEAD(0) or ALIVE (1) to each cell of the grid */
void generateGrid(int **grid, int nrows, int ncolumns) {
  clearBorder(grid, nrows, ncolumns);
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  srand(time(0)); // use current time as seed for random generator //TODO
  for (int i = 1; i < endRow; i++) {
    for (int j = 1; j < endColumn; j++) {
      grid[i][j] = rand() % 2;
      // printf("Random value on [0,%d]: %d\n", RAND_MAX, grid[i][j]); //TODO
    }
  }
}

void computeNextGrid(int **current_grid, int **next_grid, int startRow,
                     int endRow, int startColumn, int endColumn) {
  // For each cell compute its next life state (DEAD/ALIVE) based on its
  // neighbours
  int up, down, left, right, nsum;
#pragma omp parallel for num_threads(thread_count) private(                    \
    up, down, left, right, nsum) schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = startRow; i <= endRow; i++) {
    for (int j = startColumn; j <= endColumn; j++) {
      up = i - 1;
      down = i + 1;
      left = j - 1;
      right = j + 1;

      nsum = current_grid[up][left] + current_grid[up][j] +
             current_grid[up][right] + current_grid[i][left] +
             current_grid[i][right] + current_grid[down][left] +
             current_grid[down][j] + current_grid[down][right];

      switch (nsum) {
      case 3:
        next_grid[i][j] = ALIVE;
        break;

      case 2:
        next_grid[i][j] = current_grid[i][j];
        break;

      default:
        next_grid[i][j] = DEAD;
      }
    }
  }
}

int main(int argc, char *argv[]) {

  struct timeval startTime;
  struct timeval stopTime;
  float elapsedTimeMilliSec, elapsedTimeSec;
  gettimeofday(&startTime, 0);

  if (argc < 5) {
    printf("Not enough arguments! %d\n", argc);
    printf(
        "Usage: %s numberOfGridRows numberOfGridColumns "
        "numberOfSimulationSteps numberOfThreads [initialGridStateFile.csv]\n",
        PROGRAM_NAME);
    return 0;
  }

  int initRows = atoi(argv[1]);
  int initColumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);
  thread_count = atoi(argv[4]);

  int nrows, ncolumns;
  int **current_grid, **next_grid;

  // allocate arrays
  nrows = initRows;       // add 2 for left and right ghost cells
  ncolumns = initColumns; // add 2 for top and bottom ghost cells
  int gridTopRow = 1, gridBottomRow = nrows - 2;
  int gridLeftColumn = 1, gridRightColumn = ncolumns - 2;
  current_grid = malloc(nrows * sizeof(int *));
  next_grid = malloc(nrows * sizeof(int *));

  for (int i = 0; i < nrows; i++) {
    current_grid[i] = malloc(ncolumns * sizeof(int));
    next_grid[i] = malloc(ncolumns * sizeof(int));
  }

  // pupulate initial grid
  if (argc == 6) {
    char *initialGridFileName = argv[5];
    clearBorder(current_grid, nrows, ncolumns);
    fileToGrid(initialGridFileName, current_grid, nrows, ncolumns, WITH_BORDER);
  } else {
    generateGrid(current_grid, nrows, ncolumns);
  }
  clearBorder(next_grid, nrows, ncolumns);

  // create matrix file
  // gridToFile(PROGRAM_NAME, current_grid, 0, nrows, ncolumns, WITH_BORDER);

  // create imgage file from matrix
  gridToImageFile(PROGRAM_NAME, current_grid, 0, nrows, ncolumns, WITH_BORDER);

  //  time steps
  for (int currentStep = 1; currentStep <= nsteps; currentStep++) {
    computeNextGrid(current_grid, next_grid, gridTopRow, gridBottomRow,
                    gridLeftColumn, gridRightColumn);
    // create matrix file
    // gridToFile(PROGRAM_NAME, next_grid, currentStep, nrows, ncolumns,
    //            WITH_BORDER);

    // create image file
    gridToImageFile(PROGRAM_NAME, next_grid, currentStep, ncolumns, nrows,
                    WITH_BORDER);

    // copy next_grid state into current_grid state
    int **temp = current_grid;
    current_grid = next_grid;
    next_grid = temp;
  }

  // Iterations are done; sum the number of live cells
  int isum = 0;
#pragma omp parallel for num_threads(thread_count)                             \
    reduction(+ : isum) schedule(SCHED_TYPE, SCHED_CHUNK_SIZE)
  for (int i = gridTopRow; i <= gridBottomRow; i++) {
    for (int j = gridLeftColumn; j <= gridRightColumn; j++) {
      isum += current_grid[i][j];
    }
  }
  printf("\nNumber of live cells = %d\n", isum);

  free(current_grid);
  free(next_grid);

  gettimeofday(&stopTime, 0);
  // elapsedTime in milliseconds
  elapsedTimeMilliSec = (stopTime.tv_sec - startTime.tv_sec) * 1000.0f +
                        (stopTime.tv_usec - startTime.tv_usec) / 1000.0f;
  elapsedTimeSec = elapsedTimeMilliSec / 1000.0f;
  printf("Code executed in: %.3f seconds = %.3f milliseconds.\n",
         elapsedTimeSec, elapsedTimeMilliSec);

  return 0;
}
