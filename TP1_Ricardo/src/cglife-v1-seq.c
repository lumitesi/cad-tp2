/**
* CAD 2015/2016 - TP1
* Ricardo Gaspar, nr 42038
* Conway's Game of Life
* Sequential Version - Version 1:
* The cells in the border of the grid are always considered to be dead.
*/

#include "file2ppm.c"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define PROGRAM_NAME "cglife-v1-seq"

// Grid size
#define DEFAULT_NROWS 2048
#define DEFAULT_NCOLUMNS 2048
// number of time steps
#define DEFAULT_NSTEPS 100

#define WITH_BORDER 1
#define WITHOUT_BORDER 0

enum CELL_STATES { DEAD = 0, ALIVE };

void clearBorder(int **grid, int nrows, int ncolumns) {
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  // left-right border
  for (int i = 0; i < nrows; i++) {
    grid[i][0] = DEAD;
    grid[i][endColumn] = DEAD;
  }

  // top-bottom border
  for (int j = 0; j < ncolumns; j++) {
    grid[0][j] = DEAD;
    grid[endRow][j] = DEAD;
  }
}

/*  randomly generate values DEAD(0) or ALIVE (1) to each cell of the grid */
void generateGrid(int **grid, int nrows, int ncolumns) {
  clearBorder(grid, nrows, ncolumns);
  int endRow = nrows - 1;
  int endColumn = ncolumns - 1;
  srand(time(0)); // use current time as seed for random generator //TODO
  for (int i = 1; i < endRow; i++) {
    for (int j = 1; j < endColumn; j++) {
      grid[i][j] = rand() % 2;
      // printf("Random value on [0,%d]: %d\n", RAND_MAX, grid[i][j]); //TODO
    }
  }
}

void computeNextGrid(int **current_grid, int **next_grid, int startRow,
                     int endRow, int startColumn, int endColumn) {
  // For each cell compute its next life state (DEAD/ALIVE) based on its
  // neighbours
  int up, down, left, right, nsum;
  for (int i = startRow; i <= endRow; i++) {
    for (int j = startColumn; j <= endColumn; j++) {
      up = i - 1;
      down = i + 1;
      left = j - 1;
      right = j + 1;

      nsum = current_grid[up][left] + current_grid[up][j] +
             current_grid[up][right] + current_grid[i][left] +
             current_grid[i][right] + current_grid[down][left] +
             current_grid[down][j] + current_grid[down][right];

      switch (nsum) {
      case 3:
        next_grid[i][j] = ALIVE;
        break;

      case 2:
        next_grid[i][j] = current_grid[i][j];
        break;

      default:
        next_grid[i][j] = DEAD;
      }
    }
  }
}

int main(int argc, char *argv[]) {

  struct timeval startTime;
  struct timeval stopTime;
  float elapsedTimeMilliSec, elapsedTimeSec;
  gettimeofday(&startTime, 0);

  if (argc < 4) {
    printf("Not enough arguments! %d\n", argc);
    printf("Usage: %s numberOfGridRows numberOfGridColumns "
           "numberOfSimulationSteps [initialGridStateFile.csv]\n",
           PROGRAM_NAME);
    return 0;
  }

  int initRows = atoi(argv[1]);
  int initColumns = atoi(argv[2]);
  int nsteps = atoi(argv[3]);

  int nrows, ncolumns;
  int **current_grid, **next_grid;

  // allocate arrays
  nrows = initRows;       // add 2 for left and right ghost cells
  ncolumns = initColumns; // add 2 for top and bottom ghost cells
  current_grid = malloc(nrows * sizeof(int *));
  next_grid = malloc(nrows * sizeof(int *));

  int i, j;
  for (i = 0; i < nrows; i++) {
    current_grid[i] = malloc(ncolumns * sizeof(int));
    next_grid[i] = malloc(ncolumns * sizeof(int));
  }

  // pupulate initial grid
  if (argc == 5) {
    char *initialGridFileName = argv[4];
    clearBorder(current_grid, nrows, ncolumns);
    fileToGrid(initialGridFileName, current_grid, nrows, ncolumns, WITH_BORDER);
  } else {
    generateGrid(current_grid, nrows, ncolumns);
  }
  clearBorder(next_grid, nrows, ncolumns);

  // create matrix file
  // gridToFile(PROGRAM_NAME, current_grid, 0, nrows, ncolumns, WITH_BORDER);

  // create imgage file from matrix
  gridToImageFile(PROGRAM_NAME, current_grid, 0, nrows, ncolumns, WITH_BORDER);

  int currentStep;
  //  time steps
  for (currentStep = 0; currentStep < nsteps; currentStep++) {
    computeNextGrid(current_grid, next_grid, 1, nrows - 2, 1, ncolumns - 2);
    // create matrix file
    // gridToFile(PROGRAM_NAME, next_grid, currentStep + 1, nrows, ncolumns,
    //            WITH_BORDER);

    // create image file
    gridToImageFile(PROGRAM_NAME, next_grid, currentStep + 1, ncolumns, nrows,
                    WITH_BORDER);

    // copy next_grid state into current_grid state
    int **temp = current_grid;
    current_grid = next_grid;
    next_grid = temp;
  }

  // Iterations are done; sum the number of live cells
  int isum = 0;
  int gridTopRow = 1, gridBottomRow = nrows - 2;
  int gridLeftColumn = 1, gridRightColumn = ncolumns - 2;
  for (i = gridTopRow; i <= gridBottomRow; i++) {
    for (j = gridLeftColumn; j <= gridRightColumn; j++) {
      isum = isum + current_grid[i][j];
    }
  }
  printf("\nNumber of live cells = %d\n", isum);

  free(current_grid);
  free(next_grid);

  gettimeofday(&stopTime, 0);
  // elapsedTime in milliseconds
  elapsedTimeMilliSec = (stopTime.tv_sec - startTime.tv_sec) * 1000.0f +
                        (stopTime.tv_usec - startTime.tv_usec) / 1000.0f;
  elapsedTimeSec = elapsedTimeMilliSec / 1000.0f;
  printf("Code executed in: %.3f seconds = %.3f milliseconds.\n",
         elapsedTimeSec, elapsedTimeMilliSec);

  return 0;
}
