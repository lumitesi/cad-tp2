# file2ppm Compile and Run

###### Compile
```gcc -g -Wall -o file2ppm file2ppm.c
```

###### Run

```sh 
./file2ppm dim_x dim_y fich_int fich_ppm step
```
