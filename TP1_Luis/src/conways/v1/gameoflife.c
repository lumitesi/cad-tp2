//
// Conway's Game of Life V1
// Luis Silva - 44890
//
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define ROWS_DEFAULT 10
#define COLS_DEFAULT 10
#define CELL_ALIVE 1
#define CELL_DEAD 0

int rows;
int cols;
int num_Generations;
char outputName[30];
char inputFile[30];
int num_Threads;

// Print grid
void printGrid(int **grid) {

  printf("\n");
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      printf("%d ", grid[row][col]);
    }
    printf("\n");
  }
  printf("\n");
}

// Clear grid (write all 0's)
void clearGrid(int **grid) {
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      grid[row][col] = CELL_DEAD;
    }
  }
}

// Frees the memory
void freeMatrix(int **grid) {
#pragma omp parallel for num_threads(num_Threads)
  for (int row = 0; row < rows; row++) {
    free(grid[row]);
  }
  free(grid);
}

// Copys all the positions from a matrix to the other
void copyMatrix(int **grid, int **new_grid) {
  int row, col;
#pragma omp parallel for num_threads(num_Threads) private(col)
  for (row = 0; row < rows; row++) {
    for (col = 0; col < cols; col++) {
      grid[row][col] = new_grid[row][col];
    }
  }
}

// Generate random grid
void randomizeGrid(int **grid) {
  time_t t;
  srand((unsigned)time(&t));

  // clearGrid(grid);
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      // if (rand() % 5 == 0) {
      if (rand() % 10 == 0) {
        grid[row][col] = CELL_ALIVE;
      }
    }
  }
}

// Save grid to file
void saveGridToFile(int **grid, int generation) {
  char outputNameMatrix[30];
  sprintf(outputNameMatrix, "%s_%03d.matrix", outputName, generation);
  FILE *f = fopen(outputNameMatrix, "w");

  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      fprintf(f, "%d ", grid[row][col]);
    }
    // New line
    fprintf(f, "\n");
  }
  fclose(f);
}

// Save grid to PPM TODO
void saveGridToPPM(int **grid, int generation) {
  char outputNamePPM[30];
  sprintf(outputNamePPM, "%s/%s_%03d.ppm", outputName, outputName, generation);
  // printf("%s\n", outputNamePPM); // TODO DEBUG

  // Creates a new folder for the ppm files
  struct stat st = {0};
  if (stat(outputName, &st) == -1) {
    mkdir(outputName, 0700);
  }

  // unsigned char data[rows][cols];
  unsigned char **data;
  data = malloc(rows * sizeof(unsigned char *));
  for (int row = 0; row < rows; row++) {
    data[row] = malloc(cols * sizeof(unsigned char));
  }

  FILE *f = fopen(outputNamePPM, "w");

  const char *comment = "# Conways pgm file";

  int row, col;
#pragma omp parallel for num_threads(num_Threads) private(col)
  for (row = 0; row < rows; row++) {
    for (col = 0; col < cols; col++) {
      switch (grid[row][col]) {
      case 0:
        data[row][col] = 0;
        break;
      case 1:
        data[row][col] = 255;
        break;
      }
    }
  }
  /* write file header */
  fprintf(f, "P5\n %s\n %d\n %d\n %d\n", comment, rows, cols, 255);
  /* write image data bytes to the file */
  // fwrite(*data, sizeof(*data), 1, f);
  for (int row = 0; row < rows; row++) {
    fwrite(data[row], sizeof(unsigned char), cols, f);
  }
  fclose(f);
  free(data);
}

void readGridFromFile(int **grid) {
  int value;
  FILE *f = fopen(inputFile, "r");

  clearGrid(grid);
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      fscanf(f, "%d", &value);
      grid[row][col] = value;
    }
  }
}

// Clear the border of the grid for V1
void clearBorder(int **grid) {
  for (int col = 0; col < cols; col++) {
    grid[0][col] = CELL_DEAD;
    grid[rows - 1][col] = CELL_DEAD;
  }
  for (int row = 0; row < rows; row++) {
    grid[row][0] = CELL_DEAD;
    grid[row][cols - 1] = CELL_DEAD;
  }
}

// Calculates the surrounding alive neighbors to a cell
int countAliveNeighbors(int **grid, int cellRow, int cellCol) {
  int neighbors = 0;
  int row, col;

  // Slows down
  //#pragma omp parallel for num_threads(num_Threads) private(col) reduction(+ :
  // neighbors)
  for (row = cellRow - 1; row <= (cellRow + 1); row++) {
    for (col = cellCol - 1; col <= (cellCol + 1); col++) {
      if (row == cellRow && col == cellCol) {
        continue;
      }
      if (row < rows && col < cols && row >= 0 && col >= 0) {
        neighbors += grid[row][col];
      }
    }
  }

  return neighbors;
}

// EVOLVE
void evolve(int **grid) {
  int neighborsAlive = 0;
  int **new_grid;

  new_grid = calloc(rows, sizeof(int *));
  for (int row = 0; row < rows; row++) {
    new_grid[row] = calloc(cols, sizeof(int));
  }

  int row, col;
// Takes in account the border
#pragma omp parallel for num_threads(num_Threads) private(col, neighborsAlive)
  for (row = 1; row < rows - 1; row++) {
    for (col = 1; col < cols - 1; col++) {

      neighborsAlive = countAliveNeighbors(grid, row, col);

      if (grid[row][col] == CELL_ALIVE) {
        if (neighborsAlive > 3 || neighborsAlive < 2) {

          new_grid[row][col] = CELL_DEAD;

        } else {

          new_grid[row][col] = CELL_ALIVE;
        }
      } else {
        if (neighborsAlive == 3) {

          new_grid[row][col] = CELL_ALIVE;

        } else {

          new_grid[row][col] = CELL_DEAD;
        }
      }
    }
  }
  copyMatrix(grid, new_grid);
  freeMatrix(new_grid);
}

void gameMain() {
  // Define and initialize the grid
  // int grid[rows][cols];
  int **grid;
  int generation = 0;

  grid = calloc(rows, sizeof(int *));
  for (int row = 0; row < rows; row++) {
    grid[row] = calloc(cols, sizeof(int));
  }

  if (num_Generations == 0) {
    // printf("Randomize Grid Start\n"); // TODO DEBUG
    randomizeGrid(grid);
    printf("Randomize Grid DONE!\n"); // TODO DEBUG
    // printf("Clear Grid Border Start\n"); // TODO DEBUG
    clearBorder(grid);
    printf("Clear Grid Border DONE!\n"); // TODO DEBUG
  } else {
    readGridFromFile(grid);
  }

  // printf("Save PPM Grid Start\n"); // TODO DEBUG
  saveGridToPPM(grid, generation);
  printf("Save PPM Grid DONE!\n"); // TODO DEBUG

  // printGrid(grid); // TODO DEBUG
  saveGridToFile(grid, generation);
  printf("Save Grid to File DONE!\n"); // TODO DEBUG

  // printf("Evolve Grid Start\n"); // TODO DEBUG
  while (generation < num_Generations) {
    generation++;
    evolve(grid);
    saveGridToPPM(grid, generation);
  }
  // for (int row = 0; row < rows; row++) {
  //  free(grid[row]);
  //}
  // free(grid);
  printf("Evolve Grid DONE!\n"); // TODO DEBUG
  // saveGridToFile(grid);
  // saveGridToPPM(grid);
}

// Main function
// Reads the arguments typed on launch
int main(int argc, char **argv) {
  printf("argc value = %d\n", argc); // TODO DEBUG
  if (argc == 7) {

    rows = atoi(argv[1]);
    cols = atoi(argv[2]);
    num_Generations = atoi(argv[3]);
    strcpy(inputFile, argv[4]);
    strcpy(outputName, argv[5]);
    num_Threads = atoi(argv[6]);

  } else if (argc == 4) {

    rows = atoi(argv[1]);
    cols = atoi(argv[2]);
    strcpy(outputName, argv[3]);
    num_Generations = 0;

  } else {
    fprintf(
        stderr,
        "usage: %s rows cols num_Generations seedFile outputName num_Threads\n",
        argv[0]);
    fprintf(stderr, "usage: %s rows cols outputName\n", argv[0]);
    exit(0);
  }

  // printf("rows value = %d\n", rows);             // TODO DEBUG
  // printf("cols value = %d\n", cols);             // TODO DEBUG
  // printf("inputFile value = %s\n", inputFile);   // TODO DEBUG
  // printf("outputName value = %s\n", outputName); // TODO DEBUG

  struct timeval t1, t2;
  double elapsedTime;
  gettimeofday(&t1, NULL);

  gameMain();

  gettimeofday(&t2, NULL);
  // compute and print the elapsed time in millisec
  elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;    // sec to ms
  elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // us to ms

  printf("Time elapsed = %f ms\n", elapsedTime);
  printf("Time elapsed = %f s\n", elapsedTime / 1000);

  return 0;
}
