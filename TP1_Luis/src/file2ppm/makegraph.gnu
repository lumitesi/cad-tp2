set terminal png
set output "firegraph.png"
set key off
set xrange [-1:101]
set xlabel "Probability of a Neighbor Tree Catching on Fire"
set ylabel "Percentage of Forest that Burns"
set title "Forest Fire Simulation, 100 runs for each probability"
plot "output/100fires_0.out" with points,"output/fireaverages.out" with linespoints
quit
