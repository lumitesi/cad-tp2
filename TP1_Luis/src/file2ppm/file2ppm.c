#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


void gridToFile( char *f, int * grid, int n, int linelen, int numlines ){
   FILE * fp;
   char name[32];
   unsigned char *image;
   int i, j, *apt;

   image = calloc( linelen*numlines*3, sizeof(unsigned char));

   sprintf(name, "%s.%03d", f, n);
   fp = fopen( name, "w");

   /* Print the P6 format header */
   fprintf(fp, "P6\n%d %d\n255\n", linelen, numlines);

   apt = grid;

   for (i=0; i<numlines; i++) 
      for (j=0; j<linelen; j++){
	  switch( *apt ){
		case 0:
		  image[(linelen*i+j)*3] =  0; 
		  image[(linelen*i+j)*3+1] = 250;  
		  image[(linelen*i+j)*3+2] = 0;
                  break;
		case 1:
		  image[(linelen*i+j)*3] =  250; 
		  image[(linelen*i+j)*3+1] = 0;  
		  image[(linelen*i+j)*3+2] = 0;
                  break;
                case 2:
		  image[(linelen*i+j)*3] =  0; 
		  image[(linelen*i+j)*3+1] = 0;  
		  image[(linelen*i+j)*3+2] = 0;
          }
	  apt++;
   } 

   fwrite( image, 1, linelen*numlines*3, fp);
   fclose(fp);
   free(image);
}  
   
int * fileToGrid( char *name, int dx, int dy ){
  int l;
  int *pt;
  FILE *fint;

  l = dx * dy;
  pt = calloc( l, sizeof(int));

  fint = fopen( name, "r");
  fread( pt, sizeof(int), l, fint);
  return pt;
}

int main(int argc, char * argv[]) {
  int *grid;
  int dim_x, dim_y, it;

  if (argc != 6 ){
    printf("Usage: %s dim_x dim_y fich_int fich_ppm step \n", argv[0]);
    return 1;
  }

  dim_x = atoi(argv[1]);
  dim_y = atoi(argv[2]);
  it = atoi(argv[5]);
  
  grid = fileToGrid( argv[3], dim_x, dim_y ); 
  gridToFile( argv[4], grid, it, dim_x, dim_y);

  return 0;
}



